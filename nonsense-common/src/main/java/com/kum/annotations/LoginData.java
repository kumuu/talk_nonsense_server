package com.kum.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @version V1.0
 * @Package com.kum.annotations
 * @auhter 枯木Kum
 * @date 2021/6/6-2:46 PM
 * <p>...</p>
 */

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface LoginData {
}
