package com.kum.domain.constant;

/**
 * @version V1.0
 * @Package com.kum.com.kum.domain.constant
 * @auhter 枯木Kum
 * @date 2021/4/23-4:00 PM
 */
public class ChatMsg {
    /**
     * 未读
     */
    public static final Integer UNREAD = 0;
    /**
     * 已读
     */
    public static final Integer READ = 1;



}
