package com.kum.domain.constant;

/**
 * @version V1.0
 * @Package com.kum.com.kum.domain.constant
 * @auhter 枯木Kum
 * @date 2021/4/30-5:59 PM
 * <p>...</p>
 */
public class FileUploadConstant {
    /**
     * 格式不支持
     */
    public static final String FORMAT_ERROR = "0";
    /**
     * 服务器关闭
     */
    public static final String SERVER_CLOSE = "1";
    /**
     * 上传失败
     */
    public static final String UPLOAD_FALI = "2";

}
