package com.kum.domain.constant;

/**
 * @version V1.0
 * @Package com.kum.com.kum.domain.constant
 * @auhter 枯木Kum
 * @date 2021/4/23-3:50 PM
 */
public class FriendRequest {
    /**
     * 未响应好友申请
     */
    public static final Integer No_RESPONSE = 0;
    /**
     * 同意好友申请
     */
    public static final Integer AGREE = 1;
    /**
     * 拒绝好友申请
     */
    public static final Integer REFUSE = 2;


}
