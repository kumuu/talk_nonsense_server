package com.kum.domain.constant;

/**
 * @version V1.0
 * @Package com.kum.com.kum.domain.constant
 * @auhter 枯木Kum
 * @date 2021/4/30-5:07 PM
 * <p>...</p>
 */
public class UserConstant {
    public static final String CURRENT_USER_KEY = "CURRENT_USER_KEY";
    public static final String REDIS_GEO_HASH_KEY = "REDIS_GEO_HASH_KEY";
}
