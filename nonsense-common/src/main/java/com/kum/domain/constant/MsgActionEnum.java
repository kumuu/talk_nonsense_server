package com.kum.domain.constant;

/**
 * @version V1.0
 * @Package com.kum.com.kum.domain.constant
 * @auhter 枯木Kum
 * @date 2021/5/7-4:13 PM
 * <p>发送消息的动作</p>
 */

public enum MsgActionEnum {

    CONNECT(1, "第一次(或重连)初始化连接"),
    CHAT(2, "聊天消息"),
    SIGNED(3, "消息签收"),
    EEPALIVE(4, "客户端保持心跳"),
    PULL_FRIEND(5, "拉取好友"),
    FORMAT_ERROR(6,"信息解析错误");

    public final Integer type;
    public final String content;

    MsgActionEnum(Integer type, String content) {
        this.type = type;
        this.content = content;
    }

    public Integer getType() {
        return type;
    }
}