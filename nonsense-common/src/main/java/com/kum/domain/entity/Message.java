package com.kum.domain.entity;

import com.alibaba.fastjson.JSON;
import com.kum.domain.constant.MsgActionEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @version V1.0
 * @Package com.kum.com.kum.domain.entity
 * @auhter 枯木Kum
 * @date 2021/5/7-1:56 PM
 * <p>WS消息类</p>
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    /**
     * 消息类型
     */
    private Integer type;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 数据
     */
    private Object data;

    public Message(Integer type, Object data) {
        this.type = type;
        this.data = data;
    }

    public static String send(Integer type, Object o){
        return JSON.toJSONString(new Message(type, o));
    }

    public static String connect(){
        return send(MsgActionEnum.CONNECT.type,"连接成功");
    }

    public String getData() {
        return data.toString();
    }
}
