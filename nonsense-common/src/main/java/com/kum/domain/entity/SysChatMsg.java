package com.kum.domain.entity;

import com.kum.domain.constant.ChatMsg;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @version V1.0
 * @Package com.kum.com.kum.domain.entity
 * @auhter 枯木Kum
 * @date 2021/4/23-3:59 PM
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysChatMsg {
    /**
     * 聊天信息ID
     */
    private String id;
    /**
     * 发送请求用户ID
     */
    private String sendUserId;
    /**
     * 被请求用户ID
     */
    private String acceptUserId;
    /**
     * 聊天信息
     */
    private String msg;

    /**
     * 状态
     * @see ChatMsg
     */
    private Integer status = ChatMsg.UNREAD;


}
