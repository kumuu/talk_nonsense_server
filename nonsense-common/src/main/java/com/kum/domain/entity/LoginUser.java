package com.kum.domain.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @version V1.0
 * @Package com.kum.com.kum.domain.entity
 * @auhter 枯木Kum
 * @date 2021/4/30-5:02 PM
 * <p>...</p>
 */

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class LoginUser {
    /**
     * 用户ID
     */
    @ApiModelProperty("用户ID")
    private String id;
    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String userName;
    /**
     * 用户信息
     */
    private SysUserInfo info;

    public String NonsenseCode(){
        return info.getNonsenseCode();
    }


}
