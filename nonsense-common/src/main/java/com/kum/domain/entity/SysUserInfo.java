package com.kum.domain.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @version V1.0
 * @Package com.kum.com.kum.domain.entity
 * @auhter 枯木Kum
 * @date 2021/4/29-9:56 AM
 * <p>...</p>
 */

@Builder(toBuilder = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysUserInfo {
    /**
     * 用户信息ID
     */
    @ApiModelProperty("用户信息ID")
    private String id;
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 用户性别
     */
    private String sex;
    /**
     * 用户手机号
     */
    private String phone;
    /**
     * 昵称
     */
    @ApiModelProperty("昵称")
    private String nickName;
    /**
     * 用户头像
     */
    @ApiModelProperty("用户头像")
    private String faceImage;
    /**
     * 胡说号(类似于微信号)
     */
    @ApiModelProperty("胡说号(类似于微信号)")
    private String nonsenseCode;


}
