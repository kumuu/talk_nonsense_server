package com.kum.domain.dto;

import lombok.Data;

/**
 * @version V1.0
 * @Package com.kum.com.kum.domain.dto
 * @auhter 枯木Kum
 * @date 2021/4/25-2:03 PM
 */

@Data
public class SysUserDto {
    /**
     * 用户ID
     */
    private String id;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 用户头像
     */
    private String faceImage;


}
