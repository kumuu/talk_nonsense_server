package com.kum.handler;

import com.kum.annotations.LoginData;
import com.kum.annotations.UserId;
import com.kum.utils.RequestUtils;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * @version V1.0
 * @Package com.kum.handler
 * @auhter 枯木Kum
 * @date 2021/6/6-1:59 PM
 * <p>...</p>
 */

public class TokenDataExplainHandler implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter parameter) {

        return parameter.hasParameterAnnotation(UserId.class) ||
                parameter.hasParameterAnnotation(LoginData.class);

    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        if (parameter.hasParameterAnnotation(UserId.class)) {
            return RequestUtils.getCurrentLoginUserId();
        } else if ( parameter.hasParameterAnnotation(LoginData.class)) {
            return RequestUtils.getCurrentLoginUser();
        }
        return null;
    }
}
