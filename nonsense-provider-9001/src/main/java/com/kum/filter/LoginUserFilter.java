package com.kum.filter;

import com.kum.domain.constant.UserConstant;
import com.kum.domain.entity.LoginUser;
import com.kum.utils.JwtUtil;
import com.kum.utils.RequestUtils;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @version V1.0
 * @Package com.kum.filter
 * @auhter 枯木Kum
 * @date 2021/5/15-3:11 PM
 * <p>...</p>
 */

@Component
public class LoginUserFilter implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        if(token != null && JwtUtil.verity(token)){
            LoginUser loginUser = JwtUtil.getPayload(request.getHeader("token"));
            RequestUtils.setCurrentLoginUser(loginUser);
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

//        RequestUtils.Forbidden();
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
