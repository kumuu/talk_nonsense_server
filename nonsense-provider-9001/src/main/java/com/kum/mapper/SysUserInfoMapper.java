package com.kum.mapper;

import com.kum.domain.entity.SysUser;
import com.kum.domain.entity.SysUserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @version V1.0
 * @Package com.kum.mapper
 * @auhter 枯木Kum
 * @date 2021/5/1-3:12 PM
 * <p>...</p>
 */

@Repository
public interface SysUserInfoMapper extends JpaRepository<SysUserInfo,String> {

    public SysUserInfo findByUserId(String id);

    public List<SysUserInfo> findByNickNameLike(String name);


}
