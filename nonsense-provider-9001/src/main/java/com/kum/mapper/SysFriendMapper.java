package com.kum.mapper;

import com.kum.domain.entity.SysFriend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @version V1.0
 * @Package com.kum.mapper
 * @auhter 枯木Kum
 * @date 2021/4/26-3:28 PM
 */
@Repository
public interface SysFriendMapper extends JpaRepository<SysFriend,String> {

    public List<SysFriend> findByMyUserId(String myUserId);


}
