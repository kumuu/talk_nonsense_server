package com.kum.mapper;

import com.kum.domain.entity.SysFriendRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @version V1.0
 * @Package com.kum.mapper
 * @auhter 枯木Kum
 * @date 2021/4/30-5:26 PM
 * <p>...</p>
 */

@Repository
public interface SysFriendRequestMapper  extends JpaRepository<SysFriendRequest,String> {

    public List<SysFriendRequest> findBySendUserId(String userId);
    public List<SysFriendRequest> findByAcceptUserId(String userId);

}
