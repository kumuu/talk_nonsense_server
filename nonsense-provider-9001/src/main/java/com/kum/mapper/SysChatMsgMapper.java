package com.kum.mapper;

import com.kum.domain.entity.SysChatMsg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @version V1.0
 * @Package com.kum.mapper
 * @auhter 枯木Kum
 * @date 2021/5/8-9:36 AM
 * <p>...</p>
 */

@Repository
public interface SysChatMsgMapper extends JpaRepository<SysChatMsg, String>, JpaSpecificationExecutor {

    List<SysChatMsg> findBySendUserIdOrAcceptUserId(String sendUserId, String acceptUserId);

    @Query(value = "SELECT id,accept_user_id,send_user_id,msg,create_time,update_time,status FROM sys_chat_msg WHERE accept_user_id = ?1 and status=0", nativeQuery = true)
    List<SysChatMsg> findByAcceptUserIdAndUnRead(String acceptUserId);

    @Query(value = "SELECT id,accept_user_id,send_user_id,msg,create_time,update_time,status FROM sys_chat_msg WHERE accept_user_id = ?1 and status=0", nativeQuery = true)
    List<SysChatMsg> findByAcceptUserIdAndSendUserIdAndUnRead(String acceptUserId,String sendUserId);


}
