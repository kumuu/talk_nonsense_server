package com.kum.controller;

import com.alibaba.fastjson.JSONObject;
import com.kum.domain.AjaxResult;
import com.kum.domain.entity.PeopleNear;
import com.kum.service.PeopleNearService;
import com.kum.utils.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @version V1.0
 * @Package com.kum.controller
 * @auhter 枯木Kum
 * @date 2021/5/16-8:10 PM
 * <p>...</p>
 */


@RestController
@RequestMapping("/system/people-near")
public class PeopleNearController {

    @Autowired
    private PeopleNearService peopleNearService;


    /**
     * 向附近的人中添加数据
     *
     * @param peopleNear 附近的人实体类
     */
    @PostMapping("/add")
    public AjaxResult add(@RequestBody PeopleNear peopleNear){
        peopleNear.setUserId(RequestUtils.getCurrentLoginUserInfoId());
        peopleNearService.add(peopleNear);
        return AjaxResult.success();

    }

    /**
     * 根据距离进行检索附近的人
     *
     */
    @PostMapping("/findByDistance")
    public AjaxResult findByDistance(@RequestBody JSONObject jsonObject){

        String city = jsonObject.getString("city");
        Integer distance = jsonObject.getInteger("distance");
        Integer count = jsonObject.getInteger("count");
        String userId = RequestUtils.getCurrentLoginUserInfoId();
        return AjaxResult.success(peopleNearService.findByDistance(city,userId,distance,count));

    }



}
