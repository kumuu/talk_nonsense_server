package com.kum.controller;

import com.kum.annotations.UserId;
import com.kum.domain.constant.FriendRequest;
import com.kum.domain.entity.SysFriendRequest;
import com.kum.domain.entity.SysUser;
import com.kum.service.SysFriendRequestService;
import com.kum.service.SysFriendService;
import com.kum.domain.AjaxResult;
import com.kum.service.SysUserService;
import com.kum.utils.RequestUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.List;

/**
 * @version V1.0
 * @Package com.kum.controller
 * @auhter 枯木Kum
 * @date 2021/4/26-3:40 PM
 */

@RestController
@RequestMapping("/system/friend")
public class FriendController {

    @Autowired
    private SysFriendService friendService;
    @Autowired
    private SysFriendRequestService friendRequestService;
    @Autowired
    private SysUserService userService;

    /**
     * 获得「我的」好友
     *
     * @return
     */
    @GetMapping("/findByMyFriends")
    public AjaxResult getMyFriendList(@UserId String userId) {
        return AjaxResult.success(friendService.findByMyFriendInfo(userId));
    }

    /**
     * 模糊查询所有用户
     *
     * @param name
     * @return
     */
    @ApiOperation(value = "搜索用户", notes = "根据用户名模糊查询搜索用户")
    @GetMapping("/findByNickNameLike/{name}")
    public AjaxResult findByNickNameLike(@PathVariable("name") String name) {
        if (name == null) {
            return AjaxResult.error();
        }
        return AjaxResult.success(friendService.searchFriendByNickName(name));
    }


    @PostMapping("/request")
    @ApiOperation(value = "申请好友")
    public AjaxResult request(@RequestParam("targetId") String targetId, @UserId String userId) {
        SysFriendRequest friendRequest = new SysFriendRequest().toBuilder()
                .sendUserId(userId)
                .acceptUserId(targetId)
                .build();
        friendService.sendAddFriendRequest(friendRequest);
        return AjaxResult.success("申请成功");
    }

    @PostMapping("/response")
    @ApiOperation(value = "处理好友请求")
    public AjaxResult response(@RequestBody SysFriendRequest friendRequest) {
        if (friendRequest.getStatus().equals(FriendRequest.No_RESPONSE)) {
            return AjaxResult.error();
        }
        friendRequestService.save(friendRequest);
        return AjaxResult.success();
    }

}
