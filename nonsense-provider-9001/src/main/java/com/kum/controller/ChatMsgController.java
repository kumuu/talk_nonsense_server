package com.kum.controller;

import com.kum.annotations.UserId;
import com.kum.domain.AjaxResult;
import com.kum.domain.entity.SysChatMsg;
import com.kum.service.SysChatMsgService;
import com.kum.utils.RequestUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @version V1.0
 * @Package com.kum.controller
 * @auhter 枯木Kum
 * @date 2021/5/9-3:53 PM
 * <p>...</p>
 */

@Api(tags = {"聊天信息操作接口"})
@RestController
@RequestMapping("/system/chat-message")
public class ChatMsgController {

    @Autowired
    private SysChatMsgService chatMsgService;

    @ApiOperation(value = "获取我和我的朋友的聊天记录")
    @GetMapping("/findMyChatMsgOfFriend/{friendUserId}")
    public AjaxResult findMyChatMsgOfFriend(@PathVariable("friendUserId") String friendUserId, @UserId String userId) {
        return AjaxResult.success(chatMsgService.findMyChatMsgOfFriend(userId, friendUserId, 100));
    }

    @ApiOperation(value = "获取我的未读消息列表")
    @GetMapping("/findMyChatMsgOfUnReadData")
    public AjaxResult findMyChatMsgOfUnReadData(@UserId String userId) {
        System.out.println(userId);
        return AjaxResult.success(chatMsgService.findMyChatMsgOfUnReadData(userId));
    }

    @ApiOperation(value = "签署对应好友的未读信息")
    @GetMapping("/SignChatMsgByFriendId/{friendId}")
    public AjaxResult findMyChatMsgOfUnReadData(@PathVariable String friendId, @UserId String userId) {
        chatMsgService.signedMsgByFriendId(userId, friendId);
        return AjaxResult.success();
    }


    /**
     * 保存信息
     *
     * @param chatMsg
     * @return 消息Id
     */
    @PostMapping("/save")
    public String save(@RequestBody SysChatMsg chatMsg) {
        return chatMsgService.save(chatMsg);
    }


}
