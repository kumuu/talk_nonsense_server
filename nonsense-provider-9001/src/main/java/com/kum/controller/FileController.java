package com.kum.controller;

import com.kum.annotations.UserId;
import com.kum.domain.AjaxResult;
import com.kum.service.feign.FileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;


@Api(tags = {"文件操作接口"})
@RestController
@RequestMapping("/file")
public class FileController {


    @Resource
    private FileService fileService;

    /**
     * 上传图片
     * @param file 数据流
     * @return
     */
    @PostMapping("/upload/user-face-image")
    public AjaxResult uploadUserFaceImage(@RequestParam("file") MultipartFile file) {
        System.out.println(file.getContentType());
        return fileService.uploadUserFaceImage(file);

    }

    /**
     * 文件删除
     */
    @GetMapping("/delete/{path}")
    @ApiOperation(value = "删除存储服务器文件")
    public AjaxResult delete(@PathVariable("path") @RequestParam String path) {
        return fileService.delete(path);
    }


}
