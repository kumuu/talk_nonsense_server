package com.kum.utils;

import com.alibaba.fastjson.JSON;
import com.kum.domain.AjaxResult;
import com.kum.domain.constant.HttpStatus;
import com.kum.domain.constant.UserConstant;
import com.kum.domain.entity.LoginUser;
import com.kum.domain.entity.SysUser;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @version V1.0
 * @Package com.kum.com.kum.utils
 * @auhter 枯木Kum
 * @date 2021/3/19-4:32 PM
 */

@Log4j2
public class RequestUtils {

    private static Map<String, Object> SessionsDataMap = new HashMap<>();


    /**
     * 获取当前线程的Request对象
     *
     * @return
     */
    public static HttpServletRequest getCurrentRequest() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return requestAttributes.getRequest();
    }

    /**
     * 获取当前线程的Response对象
     *
     * @return
     */
    public static HttpServletResponse getCurrentResponse() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return requestAttributes.getResponse();
    }

    public static void setCurrentSessionAttribute(String key, Object val) {
        getCurrentRequest().getSession().setAttribute(key, val);
    }

    public static void removeCurrentSessionAttribute(String key) {
        getCurrentRequest().getSession().removeAttribute(key);
    }

    public static Object getCurrentSessionAttribute(String key) {
        return getCurrentRequest().getSession().getAttribute(key);
    }

    public static LoginUser getCurrentLoginUser() {
        LoginUser loginUser = (LoginUser) getCurrentSessionAttribute(UserConstant.CURRENT_USER_KEY);
        return loginUser;
    }

    public static LoginUser getCurrentLoginUser(String sessionId) {
        LoginUser loginUser = (LoginUser) SessionsDataMap.get(sessionId);
        return loginUser;
    }

    public static String getCurrentLoginUserId() {
        return getCurrentLoginUser().getId();
    }


    public static void setCurrentLoginUser(LoginUser loginUser) {
        SessionsDataMap.put(RequestUtils.getCurrentRequest().getSession().getId(), loginUser);
        log.info("userInfo: {} 已登录，sessionId:{}",
                loginUser.getInfo(),
                getCurrentRequest().getSession().getId());
        setCurrentSessionAttribute(UserConstant.CURRENT_USER_KEY, loginUser);
    }

    public static String getCurrentLoginUserInfoId() {
        return getCurrentLoginUser().getInfo().getId();
    }

    /**
     * 删除当前Session
     */
    public static void invalidate() {
        getCurrentRequest().getSession().invalidate();
    }

    /**
     * 响应无权限策略
     */
    public static void NoPeri() {
        String msg = com.kum.utils.StringUtils.format("请求访问：{}，认证失败，无法访问系统资源",
                getCurrentRequest().getRequestURI());
        ServletUtils.renderString(getCurrentResponse(), JSON.toJSONString(AjaxResult.error(HttpStatus.UNAUTHORIZED, msg)));
    }

    /**
     * 响应禁止访问策略
     */
    public static void Forbidden() {
        ServletUtils.renderString(getCurrentResponse(), JSON.toJSONString(AjaxResult.forbidden()));
    }
}
