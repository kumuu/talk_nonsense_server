package com.kum.service;

import com.kum.domain.entity.SysUser;
import com.kum.domain.entity.SysUserInfo;
import com.kum.mapper.SysUserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @version V1.0
 * @Package com.kum.service
 * @auhter 枯木Kum
 * @date 2021/5/1-3:13 PM
 * <p>...</p>
 */

@Service
public class SysUserInfoService {

    @Autowired
    private SysUserInfoMapper userInfoMapper;

    public SysUserInfo findById(String id){
        return userInfoMapper.findById(id).get();
    }



    /**
     * 根据用户ID寻找它的数据
     *
     * @param id
     * @return
     */
    public SysUserInfo findByUserId(String id) {
        return userInfoMapper.findByUserId(id);
    }


    public List<SysUserInfo> findByNickNameList(String nickName) {
        return userInfoMapper.findByNickNameLike("%" + nickName + "%");
    }

    public void save(SysUserInfo sysUserInfo) {
        userInfoMapper.save(sysUserInfo);
    }

    public void delete(String id) {
        userInfoMapper.deleteById(id);
    }


}

