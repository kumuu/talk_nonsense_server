package com.kum.service;

import com.kum.domain.entity.SysFriendRequest;
import com.kum.mapper.SysFriendRequestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version V1.0
 * @Package com.kum.service
 * @auhter 枯木Kum
 * @date 2021/4/30-5:27 PM
 * <p>...</p>
 */

@Service
public class SysFriendRequestService {

    @Autowired
    private SysFriendRequestMapper friendRequestMapper;

    /**
     * 获取我的加好友请求
     * @param myUserId
     * @return
     */
    public List<SysFriendRequest> findByMyRequest(String myUserId){
        return friendRequestMapper.findBySendUserId(myUserId);
    }

    /**
     * 获取我的相应好友请求
     * @param myUserId
     * @return
     */
    public List<SysFriendRequest> finByMyResponse(String myUserId){
        return friendRequestMapper.findByAcceptUserId(myUserId);
    }

    public void save(SysFriendRequest sysFriendRequest){
        friendRequestMapper.save(sysFriendRequest);
    }

    public void delete(String id){
        friendRequestMapper.deleteById(id);
    }









}
