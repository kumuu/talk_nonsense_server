package com.kum.service;

import com.kum.domain.dto.SysUserDto;
import com.kum.domain.entity.SysUser;
import com.kum.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @version V1.0
 * @Package com.kum
 * @auhter 枯木Kum
 * @date 2021/4/26-3:35 PM
 */

@Service
public class SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    public SysUser login(String userName, String password){
        return sysUserMapper.findByUserNameAndPassword(userName,password);
    }


    public SysUser findByMyUserId(String id){
        return sysUserMapper.findById(id).get();
    }

    public List<SysUser> findByUserNameLike(String name){
       return sysUserMapper.findByUserNameLike("%" + name + "%");
    }
    public SysUser findByUserName(String name){
        List<SysUser> list = sysUserMapper.findByUserName(name);
        if(list.size() > 0){
            return list.get(0);
        }
        return null;
    }

    public void save(SysUser sysUser){
        sysUserMapper.save(sysUser);
    }

    public void delete(String id){
        sysUserMapper.deleteById(id);
    }













}
