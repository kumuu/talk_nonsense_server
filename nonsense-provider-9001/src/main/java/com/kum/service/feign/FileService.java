package com.kum.service.feign;

import com.kum.domain.AjaxResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @version V1.0
 * @Package com.kum.service.feign
 * @auhter 枯木Kum
 * @date 2021/5/20-5:53 PM
 * <p>...</p>
 */

@FeignClient(name = "nonsense-file-service",fallback = FileServiceHystrix.class)
public interface FileService {

    @PostMapping(value = "/file/upload/user-face-image",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    AjaxResult uploadUserFaceImage(@RequestPart(value = "file",required = false) MultipartFile file);

    @GetMapping("/file/delete/{path}")
    @ApiOperation(value = "删除存储服务器文件")
    AjaxResult delete(@PathVariable(value = "path",required = false) String path);

}