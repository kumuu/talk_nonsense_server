package com.kum.service.feign;

import com.kum.domain.AjaxResult;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * @version V1.0
 * @Package com.kum.service.feign
 * @auhter 枯木Kum
 * @date 2021/5/21-2:51 PM
 * <p>...</p>
 */

@Component
public class FileServiceHystrix implements FileService {
    @Override
    public AjaxResult uploadUserFaceImage(MultipartFile file) {
        return AjaxResult.error("存储服务器不可用");
    }

    @Override
    public AjaxResult delete(String path) {
        return AjaxResult.error("存储服务器不可用");

    }
}
