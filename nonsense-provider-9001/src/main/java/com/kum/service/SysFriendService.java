package com.kum.service;

import com.kum.domain.entity.SysFriend;
import com.kum.domain.entity.SysFriendRequest;
import com.kum.domain.entity.SysUser;
import com.kum.domain.entity.SysUserInfo;
import com.kum.mapper.SysFriendMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @version V1.0
 * @Package com.kum
 * @auhter 枯木Kum
 * @date 2021/4/26-3:35 PM
 */

@Service
public class SysFriendService {

    @Autowired
    private SysFriendMapper friendMapper;
    @Autowired
    private SysFriendRequestService friendRequestService;
    @Autowired
    private SysUserInfoService userInfoService;

    public List<SysFriend> findByMyUserId(String myUserId) {
        return friendMapper.findByMyUserId(myUserId);
    }

    /**
     * 获取「我的好友」信息集合
     * @param myUserId
     * @return
     */
    public List<SysUserInfo> findByMyFriendInfo(String myUserId) {
        List<SysFriend> myFriends = findByMyUserId(myUserId);
        List<SysUserInfo> myFriendsInfos = new ArrayList<>();
        myFriends.forEach(friend ->
                myFriendsInfos.add(userInfoService.findByUserId(friend.getMyFriendUserId()))
        );
        return myFriendsInfos;
    }

    public List<SysUserInfo> searchFriendByNickName(String nickName){
        return userInfoService.findByNickNameList(nickName);
    }

    public void save(SysFriend sysFriend) {
        friendMapper.save(sysFriend);
    }

    public void delete(String id) {
        friendMapper.deleteById(id);
    }

    /**
     * 申请添加好友
     *
     * @param friendRequest
     * @// TODO: 2021/4/30  如果对方用户在线则使用WS通知
     */
    public void sendAddFriendRequest(SysFriendRequest friendRequest) {
        friendRequestService.save(friendRequest);
    }

    /**
     * 响应添加好友的事件
     *
     * @param friendRequest
     * @// TODO: 2021/4/30 使用WS通知申请好友方结果
     */
    public void sendAddFriendResponse(SysFriendRequest friendRequest) {
        friendRequestService.save(friendRequest);
    }


}
