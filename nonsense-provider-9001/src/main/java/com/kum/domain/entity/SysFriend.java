package com.kum.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * @version V1.0
 * @Package com.kum.com.kum.domain.entity
 * @auhter 枯木Kum
 * @date 2021/4/23-3:54 PM
 */


@Builder(toBuilder = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sys_friend")
public class SysFriend extends BaseEntity {
    /**
     * 好友数据表
     */
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;
    /**
     * 我的用户ID
     */
    @Column(name = "my_user_id")
    private String myUserId;
    /**
     * 我的好友ID
     */
    @Column(name = "my_friend_user_id")
    private String myFriendUserId;
}
