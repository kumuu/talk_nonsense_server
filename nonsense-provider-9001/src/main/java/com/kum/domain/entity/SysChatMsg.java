package com.kum.domain.entity;

import com.kum.domain.constant.ChatMsg;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


/**
 * @version V1.0
 * @Package com.kum.com.kum.domain.entity
 * @auhter 枯木Kum
 * @date 2021/4/23-3:59 PM
 */

@Builder(toBuilder = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sys_chat_msg")
public class SysChatMsg extends BaseEntity{
    /**
     * 聊天信息ID
     */
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;
    /**
     * 发送请求用户ID
     */
    @Column(name = "send_user_id")
    private String sendUserId;
    /**
     * 被请求用户ID
     */
    @Column(name = "accept_user_id")
    private String acceptUserId;
    /**
     * 聊天信息
     */
    @Column(name = "msg")
    private String msg;

    /**
     * 状态
     * @see com.kum.domain.constant.ChatMsg
     */
    @Column(name = "status")
    private Integer status = ChatMsg.UNREAD;


}
