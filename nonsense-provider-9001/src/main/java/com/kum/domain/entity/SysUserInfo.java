package com.kum.domain.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * @version V1.0
 * @Package com.kum.com.kum.domain.entity
 * @auhter 枯木Kum
 * @date 2021/4/29-9:56 AM
 * <p>...</p>
 */

@Builder(toBuilder = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sys_user_info")
public class SysUserInfo extends BaseEntity{
    /**
     * 用户信息ID
     */
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    @ApiModelProperty("用户信息ID")
    private String id;
    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private String userId;
    /**
     * 用户性别
     */
    @Column(name = "sex")
    private String sex;
    /**
     * 用户手机号
     */
    @Column(name = "phone")
    private String phone;
    /**
     * 昵称
     */
    @Column(name = "nick_name")
    @ApiModelProperty("昵称")
    private String nickName;
    /**
     * 用户头像
     */
    @Column(name = "face_image")
    @ApiModelProperty("用户头像")
    private String faceImage;
    /**
     * 胡说号(类似于微信号)
     */
    @Column(name = "nonsense_code")
    @ApiModelProperty("胡说号(类似于微信号)")
    private String nonsenseCode;


}
