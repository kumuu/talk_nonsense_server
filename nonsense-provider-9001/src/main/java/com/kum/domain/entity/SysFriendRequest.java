package com.kum.domain.entity;

import com.kum.domain.constant.FriendRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * @version V1.0
 * @Package com.kum.com.kum.domain.entity
 * @auhter 枯木Kum
 * @date 2021/4/23-3:46 PM
 */

@Builder(toBuilder = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sys_friend_request")
public class SysFriendRequest extends BaseEntity {
    /**
     * 请求ID
     */
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    private String id;
    /**
     * 发送请求用户ID
     */
    @Column(name = "send_user_id")
    private String sendUserId;
    /**
     * 被请求用户ID
     */
    @Column(name = "accept_user_id")
    private String acceptUserId;
    /**
     * 状态
     *
     * @see com.kum.domain.constant.FriendRequest
     */
    @Column(name = "status")
    private Integer status = FriendRequest.No_RESPONSE;


}
