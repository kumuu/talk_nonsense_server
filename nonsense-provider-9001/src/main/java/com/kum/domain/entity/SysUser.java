package com.kum.domain.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

/**
 * @version V1.0
 * @Package com.kum.com.kum.domain.entity
 * @auhter 枯木Kum
 * @date 2021/4/23-2:24 PM
 */

@Builder(toBuilder = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sys_user")
public class SysUser extends BaseEntity{
    /**
     * 用户ID
     */
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", length = 32)
    @ApiModelProperty("用户ID")
    private String id;
    /**
     * 用户名
     */
    @Column(name = "user_name")
    @NotBlank(message = "用户名不得为空")
    @ApiModelProperty("用户名")
    private String userName;
    /**
     * 密码
     */
    @NotBlank(message = "密码不得为空")
    @Column(name = "password")
    @ApiModelProperty("密码")
    private String password;
    /**
     * 用户类型
     */
    private Integer type;




}
