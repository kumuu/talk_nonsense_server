package com.kum.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @version V1.0
 * @Package com.kum.com.kum.domain.entity
 * @auhter 枯木Kum
 * @date 2021/5/16-8:12 PM
 * <p>...</p>
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PeopleNear {

    /**
     * 用户ID
     */
    private String userId;
    /**
     * 城市
     */
    private String city;
    /**
     * 经度
     */
    private Double lng;

    /**
     * 纬度
     */
    private Double lat;


}
