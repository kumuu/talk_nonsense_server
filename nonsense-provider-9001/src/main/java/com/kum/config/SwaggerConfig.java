package com.kum.config;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

/**
 * @version V1.0
 * @Package com.lvtc.config
 * @auhter 枯木Kum
 * @date 2021/3/30-7:14 PM
 */

@Configuration
@EnableSwagger2
@EnableSwaggerBootstrapUI
public class SwaggerConfig {

    @Bean
    public Docket docket() {
        return new Docket(SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.kum.controller"))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfo("lvtc-ncre register server",
                "全国计算机等级考试报名系统后端api文档",
                "1.0",
                "urn:tos",
                new Contact("Kum", "https://ku-m.cn/", "k@ku-m.cn"),
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList());

    }
}
