package com.kum.config;

import com.kum.handler.TokenDataExplainHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @version V1.0
 * @Package com.kum.config
 * @auhter 枯木Kum
 * @date 2021/5/1-1:09 PM
 * <p>...</p>
 */

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {


    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(tokenDataExplainHandler());
    }

    @Bean
    public TokenDataExplainHandler tokenDataExplainHandler(){
        return new TokenDataExplainHandler();
    }
}
