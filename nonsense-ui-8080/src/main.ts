import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import Vant from 'vant';
import 'vant/lib/index.css';
import { Notify } from 'vant';
// createApp(App).use(Vant, Notify, router).mount('#app')
const vue = createApp(App)
vue.use(router)
vue.use(Vant)
vue.use(Notify)
vue.mount('#app')

