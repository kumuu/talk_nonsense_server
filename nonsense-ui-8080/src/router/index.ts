import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/chating/:firendId',
    name: 'chating',
    component: () => import('../views/Chating.vue')
  },
  {
    path: '/people_near',
    name: 'people_near',
    component: () => import('../views/PeopleNear.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
