import api from '@/api/index'

export const login = (data: any) => {
  return api.post('/user/login', data)
}
export const findByMyFriends = (): any => {
  return api.get('friend/findByMyFriends')
}
export const searchFriendsByNickName = (nickName: string): any => {
  return api.get(`friend/findByNickNameLike/${nickName}`)
}


export const findMyChatMsgOfFriend = (friendUserId: string): any => {
  return api.get(`chat-message/findMyChatMsgOfFriend/${friendUserId}`)
}

export const findMyChatMsgOfUnReadData = () => {
  return api.get('chat-message/findMyChatMsgOfUnReadData')
}


export const addPeopleNear = (data: {
  city: string,
  lng: number,
  lat: number
}) => {
  return api.post('people-near/add', data)

}
export const findByDistance = (data: {
  city: string,
  distance: number,
  count: number
}) => {
  return api.post('people-near/findByDistance', data)

}
