# 工程简介

### 项目状态

最近一直处于学习状态，不定时的都会对项目整体进行重构，引入新的技术或者重构某个模块，或有了更好的思路…等。所以，本项目仅供于学习和参考。

### 项目概述

本项目主要的实现方向在于能够实现多用户基于网络互相达到即时通信的目的。而作为一个即时通信软件必将承受高额的网络IO, 因此本项目采用分布式微服务 + 前后端分离的架构。

前端使用Vue3 + TypeScript，后端采用SpringCloud + SpringBoot + Spring Data JPA 。基于 Netty 使用 WebSocket 实现了正常的C/S通信模块。根据场景实现了：**基础聊天、附近的人、个人信息编辑**。并对各个模块拆分为微服务架构。

主要技术点:

1. 基于Netty NIO形式通过 WebSocket 协议进行通讯

2. 基于Redis GeoHash 数据结构实现检索用户「附近的人」

3. 基于Docker搭建FastDFS存储服务器并实现对应接口

4. 基于策略模式+工厂模式解除WebSocket的信息处理类的耦合

5. 基于SpringCloud使用Nacos + Getway + OpenFeign实现微服务注册与互相调用

6. 基于Hystrix 熔断器对部分接口做熔断处理策略

# 演示图片

### 登录界面

![1](./project_img/1.png)

### 主界面

![2](./project_img/2.png)

### 好友列表界面

![3](./project_img/3.png)

### 聊天界面

![4](./project_img/4.png)

### 附近的人界面

![5](./project_img/5.png)