package com.kum.utils;

import com.alibaba.fastjson.JSON;
import com.kum.domain.constant.MsgActionEnum;
import com.kum.domain.entity.Message;
import com.kum.netty.UserChannelManage;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import jdk.nashorn.internal.runtime.logging.Logger;
import lombok.extern.log4j.Log4j2;

/**
 * @version V1.0
 * @Package com.kum.com.kum.utils
 * @auhter 枯木Kum
 * @date 2021/5/7-4:40 PM
 * <p>...</p>
 */

@Log4j2
public class ChannelNoticeUtils {


    public static void sendMessage(String acceptUserId,Object data){
        Channel targetChannel = UserChannelManage.get(acceptUserId);
        if(targetChannel == null){
            // 对方不在线
            log.info("send a message to to {} ,target not online",acceptUserId);
            return;
        }
        log.info("send a message to to {}",acceptUserId);
        targetChannel.writeAndFlush(new TextWebSocketFrame(
                Message.send(MsgActionEnum.CHAT.type,data)
        ));

    }
    public static void sendMessage(Channel channel,Integer type,Object data){
        channel.writeAndFlush(new TextWebSocketFrame(
                Message.send(type,data)
        ));
    }
    public static void sendMessage(Channel channel,MsgActionEnum actionEnum){
        channel.writeAndFlush(new TextWebSocketFrame(
                Message.send(actionEnum.type,actionEnum.content)
        ));
    }




}
