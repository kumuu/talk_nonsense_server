package com.kum;

import com.alibaba.fastjson.JSON;
import com.kum.netty.WebSocketServer;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * @version V1.0
 * @Package com.kum
 * @auhter 枯木Kum
 * @date 2021/4/23-4:18 PM
 */

@Component
public class NettyBooter implements ApplicationListener<ContextRefreshedEvent> {
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        WebSocketServer.getInstance().start();
    }
}
