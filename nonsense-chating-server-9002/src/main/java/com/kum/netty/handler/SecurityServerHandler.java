package com.kum.netty.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.FullHttpMessage;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.util.AttributeKey;

/**
 * @version V1.0
 * @Package com.kum.handler
 * @auhter 枯木Kum
 * @date 2021/5/11-8:47 PM
 * <p>...</p>
 */
public class SecurityServerHandler extends ChannelInboundHandlerAdapter {


    public static final AttributeKey<String> sessionId =
            AttributeKey.valueOf("sessionId");

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof FullHttpMessage) {
            // 从请求头中获取sessionID
            HttpHeaders headers = ((FullHttpMessage) msg).headers();
            String cookie = headers.get("cookie");
            if(cookie != null){
                ctx.channel().attr(sessionId).set(cookie.split("=")[1]);
            }
        }
        //other protocols
        super.channelRead(ctx, msg);
    }


}