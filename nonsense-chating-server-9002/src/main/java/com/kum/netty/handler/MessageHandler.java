package com.kum.netty.handler;

import com.kum.domain.entity.Message;
import io.netty.channel.Channel;

/**
 * @version V1.0
 * @Package com.kum.netty.handler
 * @auhter 枯木Kum
 * @date 2021/5/7-1:59 PM
 * <p>...</p>
 */
public interface MessageHandler {
    void operation( Channel channel,Message message);

}
