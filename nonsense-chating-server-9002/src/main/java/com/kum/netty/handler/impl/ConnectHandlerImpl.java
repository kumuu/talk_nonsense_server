package com.kum.netty.handler.impl;

import com.kum.domain.entity.LoginUser;
import com.kum.domain.entity.Message;
import com.kum.netty.UserChannelManage;
import com.kum.netty.handler.MessageHandler;
import com.kum.netty.handler.SecurityServerHandler;
import com.kum.utils.ChannelNoticeUtils;
import com.kum.utils.JwtUtil;
import io.netty.channel.Channel;
import org.springframework.stereotype.Component;

/**
 * @version V1.0
 * @Package com.kum.netty.handler
 * @auhter 枯木Kum
 * @date 2021/5/7-4:18 PM
 * <p>客户端向服务端连接的处理类</p>
 */

@Component
public class ConnectHandlerImpl implements MessageHandler {

//    Autowired
//    private SysChat  @


    @Override
    public void operation(Channel channel,Message message) {
        LoginUser loginUser = JwtUtil.getPayload(message.getData().toString());
        if(loginUser != null){
            UserChannelManage.put(loginUser.getId(),channel);
        }

//        ChannelNoticeUtils.sendMessage(data,Message.connect());
    }
}
