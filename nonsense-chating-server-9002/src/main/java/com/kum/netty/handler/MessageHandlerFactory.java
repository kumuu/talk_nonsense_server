package com.kum.netty.handler;

import com.kum.domain.constant.MsgActionEnum;
import com.kum.netty.handler.impl.ConnectHandlerImpl;
import com.kum.netty.handler.impl.SendMessageHandlerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @version V1.0
 * @Package com.kum.netty
 * @auhter 枯木Kum
 * @date 2021/5/7-3:16 PM
 * <p>消息处理类工厂方法</p>
 */

@Component
public class MessageHandlerFactory {
    private static Map<Integer, MessageHandler> handlerMap = new HashMap<>();

    private final ConnectHandlerImpl connectHandler;
    private final SendMessageHandlerImpl sendMessageHandler;


    @Autowired
    public MessageHandlerFactory(ConnectHandlerImpl connectHandler, SendMessageHandlerImpl sendMessageHandler) {
        this.connectHandler = connectHandler;
        this.sendMessageHandler = sendMessageHandler;
        handlerMap.put(MsgActionEnum.CONNECT.type, connectHandler);
        handlerMap.put(MsgActionEnum.CHAT.type, sendMessageHandler);
    }

    public  MessageHandler getMessageHandler(Integer type) {
        return handlerMap.get(type);
    }


}
