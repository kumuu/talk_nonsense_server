package com.kum.netty.handler.impl;

import com.kum.api.RemoteChatMsgService;
import com.kum.domain.entity.LoginUser;
import com.kum.domain.entity.Message;
import com.kum.netty.UserChannelManage;
import com.kum.netty.handler.MessageHandler;
import com.kum.utils.JwtUtil;
import io.netty.channel.Channel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @version V1.0
 * @Package com.kum.netty.handler
 * @auhter 枯木Kum
 * @date 2021/5/7-4:18 PM
 * <p>用户处理未读信息处理类</p>
 */

@Component
public class SignedHandlerImpl implements MessageHandler {

    @Resource
    private RemoteChatMsgService chatMsgService;

    @Override
    public void operation(Channel channel,Message message) {
        chatMsgService.findMyChatMsgOfUnReadData(message.getData());
    }
}
