package com.kum.netty;

import io.netty.channel.Channel;
import lombok.extern.log4j.Log4j2;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @version V1.0
 * @Package com.kum.netty
 * @auhter 枯木Kum
 * @date 2021/5/7-4:21 PM
 * <p>...</p>
 */

@Log4j2
public class UserChannelManage {
    private static Map<String, Channel> usersChannel = new ConcurrentHashMap<>();
    private static Map<String, Channel> usersData = new ConcurrentHashMap<>();

    public static void put(String userId,Channel currentChannel) {
//        String userId = RequestUtils.getCurrentLoginUser().getUser().getId();
        usersChannel.put(userId,currentChannel);
        log.info("{} 连接服务器",userId);
    }

    public static Channel get(String userId) {
        return usersChannel.get(userId);
    }

    public static void remove(String userId) {
        usersChannel.remove(userId);
    }
}
