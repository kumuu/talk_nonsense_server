package com.kum.netty;

import com.kum.netty.handler.SecurityServerHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;

/**
 * @version V1.0
 * @Package PACKAGE_NAME
 * @auhter 枯木Kum
 * @date 2021/4/21-7:07 PM
 */
public class ServerInitializer extends ChannelInitializer<SocketChannel> {

    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline pipeline = socketChannel.pipeline();
        // WebSocket 基于http协议，所以需要http编解码器
        pipeline.addLast("HttpServerCodec",new HttpServerCodec());
        // 对写大数据的支持
        pipeline.addLast(new ChunkedWriteHandler());
        // 对httpMessage进行聚合，聚合成FullHttpRequest或FullHttpResponse
        pipeline.addLast(new HttpObjectAggregator(1024*64));
        pipeline.addLast(new SecurityServerHandler());
        pipeline.addLast(new WebSocketServerProtocolHandler("/ws"));
        pipeline.addLast("customHandler",new CustomHandler());
    }



}

