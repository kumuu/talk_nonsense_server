package com.kum.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

/**
 * @version V1.0
 * @Package com.kum.netty
 * @auhter 枯木Kum
 * @date 2021/4/23-4:05 PM
 */

@Log4j2
@Component
public class WebSocketServer {


    private static class SingletionWebSocketServer {
        static final WebSocketServer instance = new WebSocketServer();
    }

    public static WebSocketServer getInstance() {
        return SingletionWebSocketServer.instance;
    }

    private EventLoopGroup mainGroup;
    private EventLoopGroup subGroup;
    private ServerBootstrap serverBootstrap;
    private ChannelFuture future;

    private WebSocketServer() {
        mainGroup = new NioEventLoopGroup();
        subGroup = new NioEventLoopGroup();
        serverBootstrap = new ServerBootstrap();
        serverBootstrap.group(mainGroup, subGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ServerInitializer());

    }

    public void start() {
        future = serverBootstrap.bind(9002);
        log.info("netty WebSocket server 启动成功, 运行在9002端口");
    }

}
