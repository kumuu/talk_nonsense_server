package com.kum.service;

import com.kum.domain.constant.FileUploadConstant;
import com.kum.utils.FileDfsUtil;
import com.kum.utils.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @version V1.0
 * @Package com.kum.service
 * @auhter 枯木Kum
 * @date 2021/4/30-5:55 PM
 * <p>...</p>
 */

@Service
public class FileService {
    @Resource
    private FileDfsUtil fileDfsUtil;


    public String uploadImage(MultipartFile file) {
        String fileName = file.getName();
        if (!file.getContentType().contains("image")) {
            return FileUploadConstant.FORMAT_ERROR;
        }
        try {
            String path = fileDfsUtil.upload(file);
            if (!StringUtils.isEmpty(path)) {
                //返回的是根目录，因此要加上图片服务器域名
                return "http://img.ku-m.cn/" + path;
            } else {
                return FileUploadConstant.UPLOAD_FALI;
            }
        } catch (Exception e) {
            return FileUploadConstant.SERVER_CLOSE;

        }

    }


    public void deleteFile(String path) {
        fileDfsUtil.deleteFile(path);
    }
}
