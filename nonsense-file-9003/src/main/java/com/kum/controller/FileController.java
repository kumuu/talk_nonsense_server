package com.kum.controller;

import com.kum.annotations.UserId;
import com.kum.domain.AjaxResult;
import com.kum.domain.constant.FileUploadConstant;
import com.kum.service.FileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@Api(tags = {"文件操作接口"})
@RestController
@RequestMapping("/file")
public class FileController {


    @Autowired
    private FileService fileService;

    /**
     * 上传图片
     * @param file 数据流
     * @return
     */
    @ApiOperation(value = "上传头像", notes = "头像图片仅支持以下格式:png、jpg、jpeg、bmp")
    @PostMapping("/upload/user-face-image")
    public AjaxResult uploadUserFaceImage(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            return AjaxResult.error("图片不能是空的");
        }
        String result = fileService.uploadImage(file);
        switch (result) {
            case FileUploadConstant.FORMAT_ERROR:
                return AjaxResult.error("服务器不支持这个图片格式");
            case FileUploadConstant.SERVER_CLOSE:
                return AjaxResult.error("图片服务器出现异常");
            case FileUploadConstant.UPLOAD_FALI:
                return AjaxResult.error("未知错误导致上传失败");
        }
        return AjaxResult.success(result);

    }

    /**
     * 文件删除
     */
    @GetMapping("/delete/{path}")
    @ApiOperation(value = "删除存储服务器文件")
    public AjaxResult delete(@PathVariable("path") @RequestParam String path) {
        fileService.deleteFile(path);
        return AjaxResult.success("操作成功");
    }
}
