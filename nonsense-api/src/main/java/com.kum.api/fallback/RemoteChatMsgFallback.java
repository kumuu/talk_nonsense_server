package com.kum.api.fallback;

import com.kum.api.RemoteChatMsgService;
import com.kum.api.entity.SysChatMsg;
import com.kum.domain.AjaxResult;
import org.springframework.stereotype.Component;

/**
 * @version V1.0
 * @Package com.kum.api.fallback
 * @auhter 枯木Kum
 * @date 2021/5/29-4:02 PM
 * <p>服务降级处理</p>
 */

@Component
public class RemoteChatMsgFallback implements RemoteChatMsgService {

    @Override
    public String save(SysChatMsg chatMsg) {
        return null;
    }

    @Override
    public AjaxResult findMyChatMsgOfUnReadData(String friendId) {
        return null;
    }
}
