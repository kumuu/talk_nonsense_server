package com.kum.api;

import com.kum.annotations.UserId;
import com.kum.api.entity.SysChatMsg;
import com.kum.api.fallback.RemoteChatMsgFallback;
import com.kum.domain.AjaxResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @version V1.0
 * @Package java.com.kum.api
 * @auhter 枯木Kum
 * @date 2021/5/29-3:57 PM
 * <p>...</p>
 */

@FeignClient(name = "nonsense-provider-service", fallback = RemoteChatMsgFallback.class)
public interface RemoteChatMsgService {

    /**
     * 保存信息
     *
     * @param chatMsg
     * @return 消息Id
     */
    @PostMapping("/system/chat-message/save")
    public String save(@RequestBody SysChatMsg chatMsg);

    /**
     * 签署对应好友的未读信息
     *
     * @param friendId
     * @return
     */
    @GetMapping("/system/chat-message/SignChatMsgByFriendId/{friendId}")
    public AjaxResult findMyChatMsgOfUnReadData(@PathVariable("friendId") String friendId);

}