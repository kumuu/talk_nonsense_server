package com.kum.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;

/**
 * @version V1.0
 * @auhter 枯木Kum
 * @date 2021/3/31-10:35 AM
 */

public class BaseEntity {
    /**
     * 创建时间
     */
    @JsonIgnore
    public Date createTime;

    /**
     * 修改时间
     */
    @JsonIgnore
    public Date updateTime;
}
